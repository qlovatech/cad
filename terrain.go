package cad

type Terrain struct {
	vertices, normals [][3]float64
	uvs               [][2]float64

	indicies [][3]uint32
}

func (t Terrain) Indicies() []uint32 {
	var retArray = make([]uint32, 0, len(t.indicies)*3)
	for _, face := range t.indicies {
		for _, index := range face {
			retArray = append(retArray, uint32(index))
		}
	}
	return retArray
}

func (t Terrain) UVs() []float32 {
	var retArray = make([]float32, 0, len(t.uvs)*3)
	for _, uv := range t.uvs {
		for _, v := range uv {
			retArray = append(retArray, float32(v))
		}
	}
	return retArray
}

func (t Terrain) Vertices() []float32 {
	var retArray = make([]float32, 0, len(t.vertices)*3)
	for _, triangle := range t.vertices {
		for _, vertex := range triangle {
			retArray = append(retArray, float32(vertex))
		}
	}
	return retArray
}

func (t Terrain) Normals() []float32 {
	var retArray = make([]float32, 0, len(t.normals)*3)
	for _, vector := range t.normals {
		for _, v := range vector {
			retArray = append(retArray, float32(v))
		}
	}
	return retArray
}

func NewTerrain(w, h float64, resolution int, heightmap func(x, y float64) float64) Terrain {
	segmentWidth := w / float64(resolution)
	segmentHeight := h / float64(resolution)

	const overscan = 4

	//Create the terrain slightly larger so that normals are correct on the edge.
	//width := w + segmentWidth*overscan*2
	//height := h + segmentHeight*overscan*2

	//widthHalf := width / 2
	//heightHalf := height / 2
	gridX := resolution + overscan*2
	gridY := resolution + overscan*2
	gridX1 := gridX + 1
	gridY1 := gridY + 1

	// Create buffers
	var terrain Terrain

	// Generate plane vertices, vertices normals and vertices texture mappings.
	for iy := 0; iy < gridY1; iy++ {
		y := float64(iy)*segmentHeight - overscan*segmentHeight
		for ix := 0; ix < gridX1; ix++ {
			x := float64(ix)*segmentWidth - overscan*segmentWidth

			terrain.vertices = append(terrain.vertices, [3]float64{x, heightmap(x, y), y})
			terrain.normals = append(terrain.normals, [3]float64{0, 0, 0})
			terrain.uvs = append(terrain.uvs, [2]float64{float64(ix) / float64(gridX), (float64(iy) / float64(gridY))})
		}
	}

	// Generate plane vertices indices for the faces
	for iy := 0; iy < gridY; iy++ {
		for ix := 0; ix < gridX; ix++ {
			a := ix + gridX1*iy
			b := ix + gridX1*(iy+1)
			c := (ix + 1) + gridX1*(iy+1)
			d := (ix + 1) + gridX1*iy

			terrain.indicies = append(terrain.indicies, [3]uint32{uint32(a), uint32(b), uint32(d)})
			terrain.indicies = append(terrain.indicies, [3]uint32{uint32(b), uint32(c), uint32(d)})
		}
	}

	//calculate the normals
	for _, index := range terrain.indicies {

		var va, vb, vc [3]float64 = terrain.vertices[index[0]],
			terrain.vertices[index[1]],
			terrain.vertices[index[2]]

		e1 := subVec(va, vb)
		e2 := subVec(vc, vb)
		no := cross(e1, e2)

		terrain.normals[index[0]] = addVec(terrain.normals[index[0]], no)
		terrain.normals[index[1]] = addVec(terrain.normals[index[1]], no)
		terrain.normals[index[2]] = addVec(terrain.normals[index[2]], no)
	}

	terrain.indicies = nil

	// Generate plane vertices indices for the faces
	for iy := overscan; iy < gridY-overscan; iy++ {
		for ix := overscan; ix < gridX-overscan; ix++ {
			a := ix + gridX1*iy
			b := ix + gridX1*(iy+1)
			c := (ix + 1) + gridX1*(iy+1)
			d := (ix + 1) + gridX1*iy

			terrain.indicies = append(terrain.indicies, [3]uint32{uint32(a), uint32(b), uint32(d)})
			terrain.indicies = append(terrain.indicies, [3]uint32{uint32(b), uint32(c), uint32(d)})
		}
	}

	for i := range terrain.normals {
		terrain.normals[i] = normalize(terrain.normals[i])
	}

	return terrain
}
