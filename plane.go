package cad

type Plane struct {
	vertices, normals [][3]float64
	uvs               [][2]float64

	indicies [][3]uint32
}

func (t Plane) Indicies() []uint32 {
	var retArray = make([]uint32, 0, len(t.indicies)*3)
	for _, face := range t.indicies {
		for _, index := range face {
			retArray = append(retArray, uint32(index))
		}
	}
	return retArray
}

func (t Plane) UVs() []float32 {
	var retArray = make([]float32, 0, len(t.uvs)*3)
	for _, uv := range t.uvs {
		for _, v := range uv {
			retArray = append(retArray, float32(v))
		}
	}
	return retArray
}

func (t Plane) Vertices() []float32 {
	var retArray = make([]float32, 0, len(t.vertices)*3)
	for _, triangle := range t.vertices {
		for _, vertex := range triangle {
			retArray = append(retArray, float32(vertex))
		}
	}
	return retArray
}

func (t Plane) Normals() []float32 {
	var retArray = make([]float32, 0, len(t.normals)*3)
	for _, vector := range t.normals {
		for _, v := range vector {
			retArray = append(retArray, float32(v))
		}
	}
	return retArray
}

func NewPlane(w, h float64, resolution int) Plane {
	segmentWidth := w / float64(resolution)
	segmentHeight := h / float64(resolution)

	gridX := resolution
	gridY := resolution
	gridX1 := gridX + 1
	gridY1 := gridY + 1

	// Create buffers
	var terrain Plane

	// Generate plane vertices, vertices normals and vertices texture mappings.
	for iy := 0; iy < gridY1; iy++ {
		y := float64(iy) * segmentHeight
		for ix := 0; ix < gridX1; ix++ {
			x := float64(ix) * segmentWidth

			terrain.vertices = append(terrain.vertices, [3]float64{x, 0, y})
			terrain.normals = append(terrain.normals, [3]float64{0, 1, 0})
			terrain.uvs = append(terrain.uvs, [2]float64{float64(ix) / float64(gridX), (float64(iy) / float64(gridY))})
		}
	}

	// Generate plane vertices indices for the faces
	for iy := 0; iy < gridY; iy++ {
		for ix := 0; ix < gridX; ix++ {
			a := ix + gridX1*iy
			b := ix + gridX1*(iy+1)
			c := (ix + 1) + gridX1*(iy+1)
			d := (ix + 1) + gridX1*iy

			terrain.indicies = append(terrain.indicies, [3]uint32{uint32(a), uint32(b), uint32(d)})
			terrain.indicies = append(terrain.indicies, [3]uint32{uint32(b), uint32(c), uint32(d)})
		}
	}

	return terrain
}
